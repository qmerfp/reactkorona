/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from "react";
import { TouchableOpacity, Image } from "react-native";

import DeviceInfo, { getDeviceId } from "react-native-device-info";
import {
  Container,
  Header,
  Content,
  ListItem,
  Body,
  Title,
  List,
  Button,
  Text
} from "native-base";

import {
  BleManager,
  BleError,
  Device,
  fullUUID,
  State,
  LogLevel
} from "react-native-ble-plx";

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      bleDevices: [],
      devicename: null,
      detailData: []
    };

    this.manager = new BleManager();
    this.setState({ devicename: DeviceInfo.getUniqueId() });
    this.device = new Device({ manager: this.manager });
  }
  UNSAFE_componentWillMount() {
    const subscription = this.manager.onStateChange(state => {
      if (state === "PoweredOn") {
        this.scanAndConnect();
        subscription.remove();
      }
    }, true);
  }

  scanAndConnect() {
    this.manager.startDeviceScan(
      null,
      { allowDuplicates: true },
      (error, device) => {
        if (error) {
          alert(error);
          // Handle error (scanning will be stopped automatically)
          return;
        }

        this.addDevices(device);
        // console.log("devicel", device.readRSSI);
      }
    );
  }

  addDevices = device => {
    if (device.id) {
      if (this.state.bleDevices.indexOf(fullUUID(device.id)) === -1) {
        this.setState({
          bleDevices: this.state.bleDevices.concat([fullUUID(device.id)])
        });
      }
    }
  };
  stop = () => {
    this.manager.stopDeviceScan();
  };
  clear = () => {
    this.setState({ bleDevices: [] });
  };

  componentDidMount() {}
  render() {
    return (
      <>
        <Container>
          <Header>
            <Body>
              <Title>Korona Cember</Title>
            </Body>
          </Header>

          <Content>
            <Text>cihaz : {DeviceInfo.getUniqueId()}</Text>

            <Image
              source={require("./assets/1042-round.gif")}
              style={{ alignSelf: "center" }}
            />

            <List
              dataArray={this.state.bleDevices}
              renderRow={item => (
                <ListItem>
                  <Text>{JSON.stringify(item)}</Text>
                </ListItem>
              )}
            ></List>
          </Content>
        </Container>
      </>
    );
  }
}

export default App;
